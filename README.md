* Include the following lines at the bottom of your index.html:

```
#!html

<script src="bower_components/urijs/src/URI.min.js"></script>
<script src="bower_components/invento-color-override/dist/js/color-override.js"></script>
<script src="bower_components/invento-whitelabel/dist/js/whitelabel.js"></script>
```

* Include the following package in your app's module:

```
#!javascript

'invento-whitelabel'
```