(function() {
    'use strict';

    var whitelabel = angular.module('invento-whitelabel', ['invento-color-override']);

    whitelabel.factory('InventoWhitelabel', function($resource, $localStorage, ENV, InventoColorOverride) {
        return {
            sellers : {root : {active : true}},

            newSeller : function() {
                return $resource(ENV.API + 'custom_products/:product/:url', {}, {
                    get : {
                        method  : 'GET',
                        headers : {'api-key' : ENV.API_KEY}
                    }
                });
            },

            override : function(uri) {
                var settings = $localStorage.sellers[uri],
                    override = InventoColorOverride.NewInstance(settings.primary_colour, settings.secondary_colour, settings.button_colour),
                    colours  = override.colors();

                document.title = settings.product_name + ' | ' + document.title;
                $('.navbar-brand').children().first().hide();
                $('.copyright-message').hide();
                $('.navbar-brand').children().last().text(settings.product_name);

                override.stylesheet([
                    {
                        className : '.form-signin .logo',
                        css : {'background-image' : 'none', 'height' : '1px'}
                    },
                    {
                        className : '.invento-guide-wrapper > .header',
                        css : {'background' : 'none'}
                    },
                    {
                        className : '.navbar-default',
                        css : {'background-color' : colours[0].default, 'border-color' : colours[0].dark, 'color' : colours[0].text.default}
                    },
                    {
                        className : '.nav > li > a:hover, .nav > li > a:focus',
                        css : {'background-color' : colours[1].default, 'color' : colours[1].text.default}
                    },
                    {
                        className : '.btn.btn-primary, .btn.btn-info:hover, .btn.btn-info:focus',
                        css : {'background-color' : colours[2].default, 'border-color' : colours[2].dark, 'color' : colours[2].text.default}
                    },
                    {
                        className : '.btn.btn-primary:hover, .btn.btn-primary:focus',
                        css : {'background-color' : colours[2].dark, 'border-color' : colours[2].vdark, 'color' : colours[2].text.dark}
                    },
                    {
                        className : '.btn.btn-info',
                        css : {'background-color' : colours[2].light, 'border-color' : colours[2].default, 'color': colours[2].text.light}
                    },
                    {
                        className : '.navbar-default a, .navbar-default .navbar-brand, .navbar-default .navbar-nav > .active > a, .navbar-default .navbar-nav > li > a',
                        css : {'color' : colours[0].text.default}
                    },
                    {
                        className : '.navbar-default a:hover, .navbar-default a:focus, .navbar-default .navbar-brand:hover, .navbar-default .navbar-brand:focus, .navbar-default .navbar-nav > .active > a, .navbar-default .navbar-nav > li > a:hover, .navbar-default .navbar-nav > li > a:focus',
                        css : {'background-color' : colours[0].light, 'color' : colours[0].text.light}
                    },
                    {
                        className : '.bootstrap-switch-primary',
                        css : {'background-color' : colours[2].pastel.default}
                    },
                    {
                        className : '.bootstrap-switch-primary ~ .bootstrap-switch-handle-off:before',
                        css : {'background-color' : colours[2].pastel.vlight}
                    },
                    {
                        className : '.bootstrap-switch-default',
                        css : {'background-color' : colours[2].pastel.light}
                    },
                    {
                        className : '.bottom-menu-inverse',
                        css : {'background-color' : colours[1].vdark, 'color' : colours[1].light}
                    },
                    {
                        className : '.dropdown-menu > li > a',
                        css : {'color': '#606d7a'}
                    },
                    {
                        className : '.modal-header',
                        css : {'background-color': colours[0].default}
                    },
                    {
                        className : '.bs-wizard > .bs-wizard-step > .bs-wizard-dot',
                        css : {'background-color': colours[0].vdark}
                    },
                    {
                        className : '.bs-wizard > .bs-wizard-step > .bs-wizard-dot:after',
                        css : {'background-color': colours[0].vlight}
                    },
                    {
                        className : '.bs-wizard > .bs-wizard-step > .bs-wizard-dot > span',
                        css : {'color': colours[0].vdark}
                    },
                    {
                        className : '.bs-wizard > .bs-wizard-step.complete > .bs-wizard-dot',
                        css : {'background-color': colours[0].vlight}
                    },
                    {
                        className : '.bs-wizard > .bs-wizard-step.disabled > .bs-wizard-dot > span',
                        css : {'color': colours[0].default}
                    },
                    {
                        className : '.bs-wizard > .bs-wizard-step > .progress',
                        css : {'background-color': colours[0].vdark}
                    },
                    {
                        className : '.bs-wizard > .bs-wizard-step > .progress > .progress-bar',
                        css : {'background-color': colours[0].vlight}
                    },
                    {
                        className : '.bs-wizard > .bs-wizard-step.active > small',
                        css : {'color': colours[0].vlight, 'text-shadow' : '2px 2px ' + colours[0].vdark}
                    },
                    {
                        className : '.bs-wizard > .bs-wizard-step.active > small:after',
                        css : {'border-color': colours[0].vdark}
                    },
                    {
                        className : '.bs-wizard > .bs-wizard-step.disabled > small',
                        css : {'color': colours[0].vdark}
                    },
                    {
                        className : '.bs-wizard > .bs-wizard-header',
                        css : {'color': colours[0].vlight, 'text-shadow' : '1px 1px ' + colours[0].vdark}
                    },
                    {
                        className : '.bs-wizard > .bs-wizard-step.complete > small',
                        css : {'color': colours[0].vlight}
                    },
                    {
                        className : '.pagination-minimal > ul > li.disabled > a, .pagination-minimal > ul > li.disabled > a:hover, .pagination-minimal > ul > li.disabled > a:focus',
                        css : {'background-color' : colours[1].vlight, opacity : 0.5}
                    },
                    {
                        className : '.pagination-minimal > ul > li.previous:not(.disabled) > a, .pagination-minimal > ul > li.next:not(.disabled) > a',
                        css : {'background-color' : colours[1].dark, 'color' : colours[1].text.dark}
                    },
                    {
                        className : '.pagination-minimal > ul > li.previous:not(.disabled) > a:hover, .pagination-minimal > ul > li.next:not(.disabled) > a:hover',
                        css : {'background-color' : colours[1].light, 'color' : colours[1].text.light}
                    },
                    {
                        className : '.pagination-minimal > ul',
                        css : {'background-color' : colours[1].default}
                    },
                    {
                        className : '.pagination-minimal > ul > li.active > a, .pagination-minimal > ul > li:not(.previous):not(.next) > a:hover, .pagination-minimal > ul > li:not(.previous):not(.next) > a:focus, .pagination-minimal > ul > li.active:not(.previous):not(.next) > a:hover, .pagination-minimal > ul > li.active:not(.previous):not(.next) > a:focus',
                        css : {'background-color' : colours[1].dark, 'border-color' : colours[1].dark, 'color' : colours[1].text.dark}
                    },
                    {
                        className : '.pagination-minimal > ul > li > a',
                        css : {'border-color' : colours[1].default}
                    },
                    {
                        className : '.btn-register',
                        css : {'display' : 'none'}
                    },
                ]);
            }
        };
    });

    whitelabel.run(function($window, $localStorage, $timeout, ENV, InventoWhitelabel) {
        var uri = new URI($window.location.href).host();

        if (!$localStorage.sellers) {
            $localStorage.sellers = InventoWhitelabel.sellers;
        }

        if (uri.indexOf(ENV.APP_BASE_URL) == -1) {
            if (!$localStorage.sellers[uri]) {
                InventoWhitelabel.newSeller().get({product : ENV.APP_NAME, url : uri}, function(success) {
                    for (var seller in $localStorage.sellers) {
                        $localStorage.sellers[seller].active = false;
                    }


                    $localStorage.sellers[uri] = {
                        active            : true,
                        primary_colour    : success.response.data.primary_colour,
                        secondary_colour  : success.response.data.secondary_colour,
                        button_colour     : success.response.data.button_colour,
                        product_name      : success.response.data.product_name,
                        expires           : new Date().getTime() + (3600 * 1000)
                    };

                    InventoWhitelabel.override(uri);

                    $timeout(function() {
                        $('.whiteout').hide();
                    }, 100);
                });
            } else {
                if ($localStorage.sellers[uri].expires < new Date().getTime()) {
                    InventoWhitelabel.newSeller().get({product : ENV.APP_NAME, url : uri}, function(success) {
                        for (var seller in $localStorage.sellers) {
                            $localStorage.sellers[seller].active = false;
                        }


                        $localStorage.sellers[uri] = {
                            active            : true,
                            primary_colour    : success.response.data.primary_colour,
                            secondary_colour  : success.response.data.secondary_colour,
                            button_colour     : success.response.data.button_colour,
                            product_name      : success.response.data.product_name,
                            expires           : new Date().getTime() + (3600 * 1000)
                        };

                        InventoWhitelabel.override(uri);

                        $timeout(function() {
                            $('.whiteout').hide();
                        }, 100);
                    });
                } else {
                    InventoWhitelabel.override(uri);

                    $timeout(function() {
                        $('.whiteout').hide();
                    }, 1000);
                }
            }
        } else {
            document.title = ENV.APP_NAME + ' | ' + document.title;
            $('.whiteout').hide();
        }
    });
})();